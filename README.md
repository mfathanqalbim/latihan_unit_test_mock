# Latihan Unit test dan Mock di Java
Disarankan menggunakan Intellij

# Instalasi
1. [Java](https://www.java.com/en/download/)
2. [JDK](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html) (Pilih sesuai OS yang digunakan)

Khusus bukan pengguna intellij
[Maven](https://maven.apache.org/download.cgi)

[Tutorial install maven](https://maven.apache.org/install.html)

```sh
3. git clone https://gitlab.com/mfathanqalbim/latihan_unit_test_mock.git
4. cd latihan_unit_test_mock
5. mvnw spring-boot:run
```

# Server List
Jalankan dulu spring boot nya untuk mengakses url dibawah
```sh
http://localhost:8123/post
http://localhost:8123/get
```
