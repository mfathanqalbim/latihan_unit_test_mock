package com.belajar.belajar.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping
@RestController
public class MainController {

    @GetMapping(value = "get")
    public String get_page(){
        return "ini get page";
    }

    @PostMapping(value = "post")
    public String post_page(){
        return "ini post page";
    }

}