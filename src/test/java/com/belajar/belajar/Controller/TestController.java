package com.belajar.belajar.Controller;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.belajar.Config.TestConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes= TestConfig.class)
public class TestController {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testGetPage() throws Exception{
        mockMvc.perform(get("/get"))
                .andExpect(status().isOk())
        .andExpect(content().string("ini get page"));
    }

    @Test
    public void testPostPage()throws Exception{
        mockMvc.perform(post("/post"))
                .andExpect(status().isOk())
                .andExpect(content().string("ini post page"));
    }

    @Test
    public void testNoGetPage()throws Exception{
        mockMvc.perform(post("/get"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testNoPostPage() throws Exception {
        mockMvc.perform(get("/post"))
                .andExpect(status().is4xxClientError());
    }
}